import React from 'react';

export const Header = () => {
    return (
        <nav>
            <div className="nav-wrapper indigo lighten-2">
            <a href="/" className="brand-logo">Logo</a>
            <ul id="nav-mobile" className="right hide-on-med-and-down">
                <li><a href="sass.html">Autores</a></li>
                <li><a href="badges.html">Livros</a></li>
                <li><a href="collapsible.html">Sobre</a></li>
            </ul>
            </div>
        </nav>
    )
}