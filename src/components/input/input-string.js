import React from 'react';

export class InputString extends React.Component {



    inputHandler = event => {
        const { value } = event.target;
        this.props.action({ [this.props.field]: value });
        
    }

    render() {

        return (
            <div className="form-control">
                <label htmlFor={ this.props.field }>{ this.props.label }</label>
                <input 
                    onChange={ this.inputHandler } 
                    type="text" 
                    name={ this.props.field }
                    id={ this.props.field } 
                    value={ this.props.value }/>
            </div>
        );
    }
}