import React, { Fragment } from 'react';
import 'materialize-css/dist/css/materialize.min.css';
import Table from './livro/table/Table';
import { LivroForm } from './livro/form/livro-form';
import { Header } from './layout/header';

export default class App extends React.Component {

  state = {
    autores: [
      {
        autor: 'Cleber',
        livro: 'React begin',
        preco: 179
      },
      {
        autor: 'Cleber',
        livro: 'React redux',
        preco: 223
      },
      {
        autor: 'Cleber',
        livro: 'React advanced',
        preco: 123
      },
      {
        autor: 'Cleber',
        livro: 'React expert',
        preco: 421
      }
    ]
  };

  removeAutor = (index) => {

    const autores = this.state.autores;
    this.setState({autores: autores.filter((item, indexAtual) => indexAtual !== index)});
  }

  addAutor = (autor) => {

    const autores = this.state.autores;
    this.setState({autores: [...autores, autor]})
  }

  render() {
    
    return (
      <Fragment>

        <Header/>
        <div className="container">
          <div className="row">
            <Table
              autores={this.state.autores} 
              removeAutor={ this.removeAutor } />
            <LivroForm
              addAutor={ this.addAutor }/>
          </div>
        </div>
        
      </Fragment>
    );
  }
}

  

