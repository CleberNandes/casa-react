import React, {Component} from 'react';


export default class Table extends Component {
    
    render() {
        const { autores, removeAutor } = this.props;
        return (
            <table className="responsive-table highlight col s6">
                <thead>
                <tr>
                    <th>Autor</th>
                    <th>Livro</th>
                    <th>Preço</th>
                    <th>Ação</th>
                </tr>
                </thead>

                <tbody>
                    {autores.map((item, index) => {
                        return (
                            <tr key={index}>
                                <td>{ item.autor }</td>
                                <td>{ item.livro }</td>
                                <td>{ item.preco }</td>
                                <td>
                                    <button onClick= { () => removeAutor(index) } >
                                        Excluir
                                    </button>
                                </td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        )
    }
}