import React from 'react';
import { InputString } from '../../components/input/input-string';

export class LivroForm extends React.Component {

    constructor(props) {
        super(props);
        this.form = {
            autor: '',
            livro: '',
            preco: ''
        }
        // eslint-disable-next-line react/no-direct-mutation-state
        this.state = this.form;
    }

    action = (item) => {
        console.log(item);
        this.setState(item);
        
    }

    create(event) {
        event.preventDefault();
        const { autor, livro, preco } = this.state;

        this.props.addAutor({autor, livro, preco})
        this.setState(this.form);

        
    }

    render() {
        const { autor, livro, preco } = this.state;
        return (
            <form onSubmit={ (event) => this.create(event) } className="col s6">

                <InputString field="autor" label="Autor" value={ autor } action={this.action}/>
                <InputString field="livro" label="Livro" value={ livro } action={this.action}/>
                <InputString field="preco" label="Preco" value={ preco } action={this.action}/>

                <button type="submit">
                    Cadastrar Livro
                </button>
            </form>
        );
    }

}